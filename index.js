require("dotenv").config({
	path: `.env.${process.env.NODE_ENV}`,
}); // Config environment
const express = require("express"); // Import express

const app = express(); // Make express app

/* Import routes */
const transactions = require("./routes/transactions");
const customers = require("./routes/customer");
const goods = require("./routes/goods");
const suppliers = require("./routes/supplier");

/* Import errorHander */
const errorHandler = require("./middlewares/errorHandler");

/* Enables req.body */
app.use(express.json()); // Enables req.body (JSON)
// Enables req.body (url-encoded)
app.use(
	express.urlencoded({
		extended: true,
	})
);

/* Use the routes */
app.use("/transactions", transactions);
app.use("/customers", customers);
app.use("/goods", goods);
app.use("/suppliers", suppliers);

/* If route not found */
app.all("*", (req, res, next) => {
	try {
		next({ message: "Endpoint not found", statusCode: 404 });
	} catch (error) {
		next(error);
	}
});

/* Use error handler */
app.use(errorHandler);

/* Run the server */

app.listen(3000, () => console.log(`Server running on 3000`));
