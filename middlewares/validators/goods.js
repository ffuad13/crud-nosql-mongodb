const validator = require("validator");
const { ObjectId } = require("mongodb");
const connection = require("../../models");

exports.createOrUpdateGoodValidator = async (req, res, next) => {
	try {
		const errorMesaages = [];

		if (!validator.isInt(req.body.price)) {
			errorMesaages.push(`Price must be a number`);
		}

		if (errorMesaages.length > 0) {
			return next({ messages: errorMesaages, statusCode: 400 });
		}

		const supplier = await connection
			.db("sales_morning")
			.collection("supplier")
			.findOne({ _id: ObjectId(req.body.id_supplier) });

		if (!supplier) {
			return next({ message: `supplier not found`, statusCode: 400 });
		}

		req.body = {
			name: req.body.name,
			supplier: eval(req.body.price),
			supplier: supplier,
		};

		next();
	} catch (error) {
		next(error);
	}
};
