const validator = require("validator");
const { ObjectId } = require("mongodb");

exports.supplierValidator = async (req, res, next) => {
  try {
    const errorMessages = [];

    if (validator.isEmpty(req.body.name)) {
      errorMessages.push("Name cannot be empty!");
    }

    if (!validator.isAlpha(req.body.name, "en-US", { ignore: " " })) {
      errorMessages.push("Please use letters only");
    }

    req.body = {
      name: req.body.name,
    };

    next();
  } catch (error) {
    next(error);
  }
};
