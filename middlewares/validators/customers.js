const validator = require("validator");

exports.createCustomerValidator = async (req, res, next) => {
  if (!validator.isAlpha(req.body.name, "en-US", { ignore: " " })) {
    return next({
      statusCode: 400,
      message: "Employee name can only contains letters",
    });
  }
  next();
};

exports.updateCustomerValidator = async (req, res, next) => {
  if (!validator.isAlpha(req.body.name, "en-US", { ignore: " " })) {
    return next({
      statusCode: 400,
      message: "Employee name can only contains letters",
    });
  }
  next();
};
