const express = require("express");

// Import validator
const { supplierValidator } = require("../middlewares/validators/supplier");
// Import controller
const {
  createSupplier,
  getSuppliers,
  getSupplierById,
  updateSupplier,
  deleteSupplier,
} = require("../controllers/supplier");

// Make router
const router = express.Router();

router.route("/").post(supplierValidator, createSupplier).get(getSuppliers);
router
  .route("/:id")
  .get(getSupplierById)
  .put(supplierValidator, updateSupplier)
  .delete(deleteSupplier);

module.exports = router;
