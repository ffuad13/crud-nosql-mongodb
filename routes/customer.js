const express = require("express");

// Import validator
const {
  createCustomerValidator,
  updateCustomerValidator,
} = require("../middlewares/validators/customers");
// Import controller
const {
  createCustomer,
  readAllCustomer,
  readDetailCustomer,
  updateCustomer,
  deleteCustomer,
} = require("../controllers/customers");

// Make router
const router = express.Router();

router
  .route("/")
  .post(createCustomerValidator, createCustomer)
  .get(readAllCustomer);
router
  .route("/:id")
  .get(readDetailCustomer)
  .put(updateCustomerValidator, updateCustomer)
  .delete(deleteCustomer);

module.exports = router;
