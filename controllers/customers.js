const { ObjectId } = require("mongodb"); // Import ObjectId
const connection = require("../models");

class Customers {
  async createCustomer(req, res, next) {
    try {
      // Insert data customer
      const newData = await connection
        .db("sales_morning")
        .collection("customers")
        .insertOne(req.body);

      const data = await connection
        .db("sales_morning")
        .collection("customers")
        .findOne({ _id: newData.insertedId });

      // If success
      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async readAllCustomer(req, res, next) {
    try {
      const data = await connection
        .db("sales_morning")
        .collection("customers")
        .find()
        .toArray();

      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async readDetailCustomer(req, res, next) {
    try {
      // Insert data customer
      const data = await connection
        .db("sales_morning")
        .collection("customers")
        .findOne({ _id: ObjectId(req.params.id) });

      // If success
      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async updateCustomer(req, res, next) {
    try {
      // Insert data customer
      const updatedData = await connection
        .db("sales_morning")
        .collection("customers")
        .updateOne(
          {
            _id: ObjectId(req.params.id),
          },
          { $set: req.body }
        );

      const data = await connection
        .db("sales_morning")
        .collection("customers")
        .findOne({ _id: ObjectId(req.params.id) });

      // If success
      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async deleteCustomer(req, res, next) {
    try {
      const deletedData = await connection
        .db("sales_morning")
        .collection("customers")
        .deleteOne({ _id: ObjectId(req.params.id) });

      if (deletedData.deletedCount === 0) {
        return next({ messages: "Customer not found", statusCode: 404 });
      }
      res
        .status(200)
        .json({ data: `Customer by id: ${req.params.id} has been deleted` });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new Customers();
