const { ObjectId } = require("mongodb");
const connection = require("../models");

class Suppliers {
  async createSupplier(req, res, next) {
    try {
      const newData = await connection
        .db("sales_morning")
        .collection("suppliers")
        .insertOne(req.body);

      const dataSupplier = await connection
        .db("sales_morning")
        .collection("suppliers")
        .findOne({ _id: newData.insertedId });

      res.status(201).json({ dataSupplier });
    } catch (error) {
      next(error);
    }
  }

  async getSuppliers(req, res, next) {
    try {
      const dataSupplier = await connection
        .db("sales_morning")
        .collection("suppliers")
        .find()
        .toArray();

      if (dataSupplier.length === 0) {
        return next({ messages: ["Suppliers not found"], statusCode: 404 });
      }

      res.status(200).json({ dataSupplier });
    } catch (error) {
      next(error);
    }
  }

  async getSupplierById(req, res, next) {
    try {
      const dataSupplier = await connection
        .db("sales_morning")
        .collection("suppliers")
        .findOne({
          _id: ObjectId(req.params.id),
        });

      if (!dataSupplier) {
        return next({ messages: ["Suppliers not found"], statusCode: 404 });
      }

      res.status(200).json({ dataSupplier });
    } catch (error) {
      next(error);
    }
  }

  async updateSupplier(req, res, next) {
    try {
      const updatedData = await connection
        .db("sales_morning")
        .collection("suppliers")
        .updateOne(
          {
            _id: ObjectId(req.params.id),
          },
          {
            $set: req.body,
          }
        );

      const dataSupplier = await connection
        .db("sales_morning")
        .collection("suppliers")
        .findOne({ _id: ObjectId(req.params.id) });

      res.status(200).json({ dataSupplier });
    } catch (error) {
      next(error);
    }
  }

  async deleteSupplier(req, res, next) {
    try {
      const deletedData = await connection
        .db("sales_morning")
        .collection("suppliers")
        .deleteOne({ _id: ObjectId(req.params.id) });

      if (deletedData.deletedCount === 0) {
        return next({ message: "Data Supplier not found", statusCode: 404 });
      }

      res
        .status(200)
        .json({ data: `Supplier with id ${req.params.id} deleted` });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new Suppliers();
