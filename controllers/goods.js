const { ObjectId } = require("mongodb");
const connection = require("../models");

class Goods {
	async createGood(req, res, next) {
		try {
			const newGood = await connection.db("sales_morning").collection("goods").insertOne(req.body);

			const data = await connection.db("sales_morning").collection("goods").findOne({ _id: newGood.insertedId });

			res.status(201).json({ data });
		} catch (error) {
			next(error);
		}
	}

	async getAllGoods(req, res, next) {
		try {
			const data = await connection.db("sales_morning").collection("goods").find().toArray();

			if (data.length === 0) {
				return next({ message: `goods not found`, statusCode: 400 });
			}

			res.status(200).json({ data });
		} catch (error) {
			next(error);
		}
	}

	async getOneGood(req, res, next) {
		try {
			let data = await connection
				.db("sales_morning")
				.collection("goods")
				.findOne({ _id: ObjectId(req.params.id) });

			if (!data) {
				return next({ message: `goods not found`, statusCode: 400 });
			}

			data.supplier = await connection
				.db("sales_morning")
				.collection("suppliers")
				.findOne({ _id: ObjectId(data.id_supplier) });

			res.status(200).json({ data });
		} catch (error) {
			next(error);
		}
	}

	async editGood(req, res, next) {
		try {
			const updated = await connection
				.db("sales_morning")
				.collection("goods")
				.updateOne({ _id: ObjectId(req.params.id) }, { $set: req.body });

			let data = await connection
				.db("sales_morning")
				.collection("goods")
				.findOne({ _id: ObjectId(req.params.id) });

			data.supplier = await connection
				.db("sales_morning")
				.collection("suppliers")
				.findOne({ _id: ObjectId(data.id_supplier) });

			res.status(201).json({ data });
		} catch (error) {
			next(error);
		}
	}

	async deleteGood(req, res, next) {
		try {
			const deletedGood = await connection
				.db("sales_morning")
				.collection("goods")
				.deleteOne({ _id: ObjectId(req.params.id) });

			if (deletedGood.deletedCount === 0) {
				return next({ message: "Good is not found", statusCode: 404 });
			}

			res.status(200).json({ data: `Good with id ${req.params.id} deleted` });
		} catch (error) {
			next(error);
		}
	}
}

module.exports = new Goods();
